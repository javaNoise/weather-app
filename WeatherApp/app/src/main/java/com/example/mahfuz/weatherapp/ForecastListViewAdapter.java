package com.example.mahfuz.weatherapp;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class ForecastListViewAdapter extends ArrayAdapter<ForecastWeather.List> {
    private Context context;
    private List<ForecastWeather.List> forecastWeatherList;
    public ForecastListViewAdapter(@NonNull Context context, @NonNull List<ForecastWeather.List> forecastWeatherList) {
        super(context, R.layout.layout_forecast_list, forecastWeatherList);
        this.context = context;
        this.forecastWeatherList = forecastWeatherList;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.layout_forecast_list, parent, false);

        TextView tvDay = convertView.findViewById(R.id.tv_day);
        TextView  tvMinTemp = convertView.findViewById(R.id.tv_temp_min);
        ImageView ivWeatherIcon = convertView.findViewById(R.id.iv_weather_icon);
        TextView tvDate = convertView.findViewById(R.id.tv_date);
        TextView tvMaxTemp = convertView.findViewById(R.id.tv_temp_max);

        String icon = "a"+forecastWeatherList.get(position).getWeather().get(0).getIcon();

        if (position == 0) {
            tvDay.setText("Today");
        } else {
            tvDay.setText(WeatherUtility.milliToDayConverter(forecastWeatherList.get(position).getDt()));
        }

        ivWeatherIcon.setImageResource(convertView.getResources().getIdentifier(icon, "drawable", "com.example.mahfuz.weatherapp"));

        tvDate.setText(forecastWeatherList.get(position).getDt()+"");
        tvMaxTemp.setText("Max: "+forecastWeatherList.get(position).getMain().getTempMax()+"°C");
        tvMinTemp.setText("Min: "+forecastWeatherList.get(position).getMain().getTempMin()+"°C");
       return convertView;
    }
}
